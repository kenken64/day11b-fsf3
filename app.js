// import the Sequelize ORM framework into your app
var Sequelize = require('sequelize');

// make a connection to your RDBMS database.
var conn = new Sequelize('employees', 'root', 'password@123',{
    host: "localhost",
    dialect: "mysql",
    pool: {
        max: 4,
        min: 0,
        idle: 10000
    }
});

conn.authenticate().then(function(result){
    console.log("Authenticate successful");
}).catch(function(err){
    console.log("Error in authentication!", err);
});


var publisher = conn.define("publisher", {
    salutation:  {
        type: Sequelize.STRING,
        defaultValue: "Mr"
    },
    name: {
        type: Sequelize.STRING,
        allowNull: true
    },
    id_no: {
        type: Sequelize.STRING
    },
    age: {
        type: Sequelize.INTEGER,
        validate: {
            ageAbove18: function(ageValue){
                if(ageValue < 18){
                    throw new Error("Only 18 and above is allow!");
                }
            }
        }
    },
    email: {
        type: Sequelize.STRING,
        unique: true,
        validate: {
            isEmail: {
                args: true,
                msg: "Invalid email"
            }
        }
    }

}, {
    timestamps: true
});


conn.sync({
    logging: console.log,
    force: true
}).then(function(){

    publisher.create({
        salutation: "Mr",
        name: "Kenneth",
        id_no: "S432892384B",
        age: 34,
        email: "k@k2.com"
    });

    publisher.create({
        salutation: "Mr",
        name: "Kenneth",
        id_no: "S432892384B",
        age: 34,
        email: "k@k3.com"
    }).then(function(){
        console.log("Inserted");
    });

    publisher.create({
        salutation: "Mr",
        name: "Kenneth",
        id_no: "S432892384B",
        age: 22,
        email: "gggg@dd.com"
    }).then(function(){
        console.log("Inserted");
    });

    publisher.create({
        name: "Kenneth",
        id_no: "S432892384B",
        age: 22,
        email: "gggg2@dd.com"
    }).then(function(){
        console.log("Inserted");
    });

    publisher.create({
        id_no: "S432892384B",
        age: 22,
        email: "gggg3@dd.com"
    }).then(function(){
        console.log("Inserted");
    });


    /*
    publisher.findAndCountAll().then(function (result){
        console.log(result);
    });*/


    publisher.count({where: ["id >? ", 0]}).then(function(result){
        console.log(result);
    })

    publisher.count().then(function(result){
        console.log(result);
    });

    publisher.bulkCreate([
        {id_no: "S432892384B",
        age: 22,
        email: "gggg11@dd.com"}
        ,
        {id_no: "S432892384B",
        age: 22,
        email: "gggg12@dd.com"}
        ,
        {id_no: "S432892384B",
        age: 22,
        email: "gggg13@dd.com"}
    ]);

});














