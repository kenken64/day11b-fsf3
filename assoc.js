/**
 * Created by phangty on 25/10/16.
 */
// import the Sequelize ORM framework into your app
var Sequelize = require('sequelize');

// make a connection to your RDBMS database.
var conn = new Sequelize('employees', 'root', 'password@123',{
    host: "localhost",
    dialect: "mysql",
    pool: {
        max: 4,
        min: 0,
        idle: 10000
    }
});

conn.authenticate().then(function(result){
    console.log("Authenticate successful");
}).catch(function(err){
    console.log("Error in authentication!", err);
});


var publisher = conn.define("publisher", {
    salutation:  {
        type: Sequelize.STRING,
        defaultValue: "Mr"
    },
    name: {
        type: Sequelize.STRING,
        allowNull: true
    },
    id_no: {
        type: Sequelize.STRING
    },
    age: {
        type: Sequelize.INTEGER,
        validate: {
            ageAbove18: function(ageValue){
                if(ageValue < 18){
                    throw new Error("Only 18 and above is allow!");
                }
            }
        }
    },
    email: {
        type: Sequelize.STRING,
        unique: true,
        validate: {
            isEmail: {
                args: true,
                msg: "Invalid email"
            }
        }
    }

}, {
    timestamps: true,
    //freezeTableName: true
});

const blogs = conn.define("blog",{
    title: {
        type: Sequelize.STRING,
        validate: {
            len: [10, 200]
        }
    },
    content: Sequelize.TEXT,
    publish_date: Sequelize.DATE,
    publisherId: {
        type: Sequelize.INTEGER,
        reference: {
            model: "publisher",
            key: "id"
        }
    }
},{
    timestamps: true
});

conn.sync({
    logging: console.log,
    force: true
}).then(function(){

/*
    publisher.create({
        salutation: "Mr",
        name: "Tan",
        id_no: "S4328322384B",
        age: 42,
        email: "teo13@dd.com"
    }).then(function(insertedPublisher){
        console.log("Inserted");
        console.log(insertedPublisher.id);
        blogs.create({
           title: "Test Kenneth 1234",
           content:" ddasdasdsadasdadasdasdasdasdsa",
           publish_date: new Date(),
           publisherId: insertedPublisher.id
        });
    });


    blogs.create({
        title: "Article 1234",
        content:" ddasdasdsadasdadasdasdasdasdsa",
        publish_date: new Date(),
        publisherId: 1
    });*/



    publisher.hasMany(blogs);
    publisher.findAll({ include: [blogs] }).then(function(p){
        console.log(p);
        p.forEach(function(item){
            console.log(item.dataValues.blogs);
        });
    });

    /*
    publisher.findById(8).then(function(result) {
        console.log(result.dataValues);
    });

    publisher.findById(3).then(function(result) {
        console.log(result.dataValues);
    });*/


    // declare transaction over the create block
    conn.transaction(function (t){ // start transaction management
        return publisher.create({ // start transaction 1
            salutation: "Mr",
            name: "Kenneth",
            id_no: "S4322384B",
            age: 40,
            email: "kenneth@dd.com"
        }, {transaction: t}).then(function (publisher){ // pass on transaction to the second call
            console.log("create blog !");
            return blogs.create({ //start transaction 2
                title: "teghjfsgfshgfsdjhfsdsf",
                content:" ddasdasdsadasdadasdasdasdasdsa",
                publish_date: new Date(),
                publisherId: publisher.id
            },{transaction: t});
        }).catch(function(err){
            console.log(err);
            throw new Error(err); //rollback
        });
    });


});
















